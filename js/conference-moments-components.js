
document.registerElement('conference-tv',{
    prototype:Object.create(HTMLElement.prototype),
    extends:'div'
});

document.registerElement('tv-button',{
    prototype:Object.create(HTMLElement.prototype),
    extends:'img'
});
