/**
 * Intel Conference Moments TV
 *
 * @author Rolando <rgarro@gmail.com>
 *
 * @requires jquery 1.11
 * @requires handlebars-v3.0.3
 * @year 2015
 */
var tv = {
    'opt':{
        'data':{},
        'dataUrl':'/gallery_json.js',
        'next':0,
        'prev':0,
        'current':0,
        'screen':'#conference-screen',
        'nextBtn':'.right-tv-button',
        'prevBtn':'.left-tv-button',
        'textContainer':'.text-container',
        'thumbContainer':'.thumbs-container',
        'thumbnailsTemplate':"{{#each photos}}<img display='{{id}}' alt='{{title}}' class='conference-thumbs tv-button img-thumbnail' src='/img/{{thumb_url}}'/>{{/each}}",
        'thumbnailsSrc':''
    },
    'loadData':function(){
        $.ajax({
            url:tv.opt.dataUrl,
            type:"GET",
            dataType:"json",
            async:false,
            success:function(data){
				tv.opt.data = data;
			 }
		  });

        },
    'setNext':function(current){
        if(current == tv.opt.data.photos.length - 1){
            tv.opt.next = 0;
        }else{
            tv.opt.next = parseInt(current) + 1;
        }
    },
    'setPrevious':function(current){
        if(current == 0){
            tv.opt.prev = parseInt(tv.opt.data.photos.length) - 1;
        }else{
            tv.opt.prev = parseInt(current) - 1;
        }
    },
    'changeTo':function(key){
        tv.opt.current = key;
        tv.setNext(key);
        tv.setPrevious(key);
    },
    'display':function(){
        var thumb_html = tv.opt.thumbnailsSrc({'photos':tv.opt.data.photos});
        $(tv.opt.thumbContainer).html(thumb_html);
        $(tv.opt.screen).attr('src','/img/gallery/' + tv.opt.data.photos[tv.opt.current].image);
        var title_html = '<b>'+ tv.opt.data.photos[tv.opt.current].title +'</b><br>'+tv.opt.data.photos[tv.opt.current].location;
        $(tv.opt.textContainer).html(title_html);
        $(tv.opt.prevBtn).attr('display',tv.opt.prev);
        $(tv.opt.nextBtn).attr('display',tv.opt.next);
    },
    'build':function(){
        tv.loadData();
        tv.opt.thumbnailsSrc = Handlebars.compile(tv.opt.thumbnailsTemplate);
        tv.opt.current = 0;
        tv.opt.next = 1;
        tv.opt.prev = tv.opt.data.photos.length - 1;
    },
    'init':function(){
        tv.build();
        tv.display();
        //clicks
        $(document).on('click','.tv-button',function(evt){
            var display = $(this).attr('display');
            tv.changeTo(display);
            tv.display();
        });
    }
};
