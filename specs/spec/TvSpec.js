describe("tv",function(){

    it("should have loadData method",function(){
        expect(tv.loadData).toBeFunction();
    });

    it("should have changeTo method",function(){
        expect(tv.changeTo).toBeFunction();
    });

    describe("tv.changeTo(2)",function(){

        beforeEach(function(){
            tv.loadData();
            tv.changeTo(2)
        });

        it("tv.opt.prev should be 1",function(){
                expect(tv.opt.prev).toBe(1);
        });

        it("tv.opt.next should be 3",function(){
                expect(tv.opt.next).toBe(3);
        });

        it("tv.opt.next should be greater than prev",function(){
                expect(tv.opt.next).toBeGreaterThan(tv.opt.prev);
        });

        it("tv.opt.next should be greater than current",function(){
                expect(tv.opt.next).toBeGreaterThan(tv.opt.current);
        });

    });

    it("should have display method",function(){
        expect(tv.display).toBeFunction();
    });

    it("should have build method",function(){
        expect(tv.build).toBeFunction();
    });

    it("should have init method",function(){
        expect(tv.init).toBeFunction();
    });

    it("should have setNext method",function(){
        expect(tv.setNext).toBeFunction();
    });

     it("should have setPrevious method",function(){
        expect(tv.setPrevious).toBeFunction();
    });

    it("should have opt hash",function(){
        expect(tv.opt).toBeObject();
    });

    describe("tv.opt",function(){

        it("should have textContainer property",function(){
            expect(tv.opt.textContainer).toBeDefined();
        });

        it("textContainer property should be string",function(){
            expect(tv.opt.textContainer).toBeString();
        });

        it("should have prevBtn property",function(){
            expect(tv.opt.prevBtn).toBeDefined();
        });

        it("prevBtn property should be string",function(){
            expect(tv.opt.prevBtn).toBeString();
        });

        it("should have nextBtn property",function(){
            expect(tv.opt.nextBtn).toBeDefined();
        });

        it("nextBtn property should be string",function(){
            expect(tv.opt.nextBtn).toBeString();
        });


        it("should have screen property",function(){
            expect(tv.opt.screen).toBeDefined();
        });

        it("screen property should be string",function(){
            expect(tv.opt.screen).toBeString();
        });


        it("should have thumbnailsTemplate property",function(){
            expect(tv.opt.thumbnailsTemplate).toBeDefined();
        });

         it("thumbnailsTemplate should be html string",function(){
            expect(tv.opt.thumbnailsTemplate).toBeHtmlString();
        });

        it("should have thumbnailsSrc property",function(){
            expect(tv.opt.thumbnailsTemplate).toBeDefined();
        });

        it("should have next property",function(){
            expect(tv.opt.next).toBeDefined();
        });

        it("next property should be number",function(){
            expect(tv.opt.next).toBeNumber();
        });

        it("should have prev property",function(){
            expect(tv.opt.prev).toBeDefined();
        });

        it("prev property should be number",function(){
            expect(tv.opt.prev).toBeNumber();
        });

        it("should have current property",function(){
            expect(tv.opt.current).toBeDefined();
        });

        it("current should be number",function(){
            expect(tv.opt.current).toBeNumber();
        });

        it("should have dataUrl property",function(){
            expect(tv.opt.dataUrl).toBeDefined();
        });

        it("dataUrl property should be string",function(){
            expect(tv.opt.dataUrl).toBeString();
        });

        it("should have data property",function(){
            expect(tv.opt.data).toBeDefined();
        });

        it("data property should be object",function(){
            expect(tv.opt.data).toBeObject();
        });

        describe("tv.opt.datUrl",function(){
            it("tv.opt.dataUrl",function(){
                expect(tv.opt.dataUrl).toBeString();
            });

        });
        describe("tv.opt.data.photos",function(){
            it("should start 0 lenght",function(){
                expect(tv.opt.data).toBeObject();
            });
             describe("calling tv.loadData()",function(){
                beforeEach(function(){
                    tv.loadData();
                });

                it("tv.opt.data.photos should have lenght > 0",function(){
                        expect(tv.opt.data.photos.length).toBeGreaterThan(0);
                });

                 it("tv.opt.data.photos[0].title should Be 'Experimental Porjectz at Intel'",function(){
                        var keys = tv.opt.data.photos[0];
                        expect(keys.title).toBe('Experimental Porjectz at Intel');
                });

            });

        });
    });

});
